# Wardrobify

Team:

* Graham Rath - Shoes
* Tyler Henderson - Hats

## Design

## Shoes microservice
Explain your models and integration with the wardrobe
microservice, here.

First thing I did was run git checkout -b grahams-branch so that I'm working on my own dev branch of our forked repo
To see what branch you're currently on use git branch.  The one with the * next to it is the one you're on

Created all the API calls in Insomnia:
    * Bins (List All, Show Details, Create New, Update Existing, Delete Existing)
    * Shoes (List All, Show Details, Create New, Update Existing, Delete Existing)

For Models:
    * First thing I did was to make a BinVO encoder from the Bin class in wardrobe_api models
    * Then created a class Shoes with the needed attributes like: (manufacturer, model name, color, URL for picture, and the bin it's in)


For Views:
    * created a ShoesListEncoder
    * created a ShoesDetailEncoder
    * created a def api_list_shoes function with "GET" and "POST" to get/display and allow the creation of Shoes
    * created a def api_show_shoes function for "DELETE" and "GET" and "PUT" to allow the listing of a single Shoes object, updating it, or deleting it
    * created a def list_bin_vos function to "GET" a list of all the BinVO's to be able to display them in the dropdown of the Add Shoes Form

## How to Run this App:
- Clone https://gitlab.com/grahamrath/microservice-two-shot/-/tree/main?ref_type=heads
- Run these commands via the terminal:
    - docker volume create two-shot-pgdata
    - docker-compose up --build
- Testing data:
    - shared_resources\tyler_hats\Insomnia_2023-12-15.json


## Hats microservice
Explain your models and integration with the wardrobe
microservice, here.

/api/hats
GET
- Allows you to display a list of hats
- example hat object:
{
    "style_name": "Shurelya's Battlesong",
    "id": 5,
    "image_url": "https://leagueofitems.com/images/items/256/2065.webp",
    "hat_color": "Gold",
    "fabric_type": "Metal",
    "import_href": "/api/locations/1/",
    "closet_name": "Renata's Closet"
}
POST
- Allows the creation of a hat
- Information needed:
    - image_url
    - style_name
    - hat_color
    - fabric_type
    - location (must be href)

/api/hats/new
GET
- Would allow a display page if site was set up to do this, all of my details
are on the list page
- Sample object is the same as /api/hats

DELETE
- Allows the deletion of a hat

PUT
- Allows you to update any information about hats

- Models:
For my models I have Hat and LocationVO. Inside of my Hat model, I have a foreign key to my LocationVO model. LocationVO is an immutable copy of the Location because I am unable to import Location from the wardrobe monolith.

-Poller:
Allows the user to access data from the Location model inside of the Wardrobe monolith

## URLS
Front-End: http://localhost:3000/
Hats API: http://localhost:8090/
Shoes API: http://localhost:8080/
Wardrobe API: http://localhost:8100/
