import React, { useState, useEffect } from 'react';

function AddHat() {
    const [locations, setLocations] = useState([]);
    const [fabricType, setFabricType] = useState([]);
    const [styleName, setStyleName] = useState([]);
    const [hatColor, setHatColor] = useState([]);
    const [imageUrl, setImageUrl] = useState([]);
    const [location, setLocation] = useState([]);

    const handleLocationChange = (event) => {
        const value = event.target.value;
        setLocation(value);
    }

    const handleFabricTypeChange = (event) => {
        const value = event.target.value;
        setFabricType(value);
    }

    const handleStyleNameChange = (event) => {
        const value = event.target.value;
        setStyleName(value);
    }

    const handleHatColorChange = (event) => {
        const value = event.target.value;
        setHatColor(value);
    }

    const handleImageUrlChange = (event) => {
        const value = event.target.value;
        setImageUrl(value);
    }

    const handleSubmit = async (event) => {
        event.preventDefault();

        const data = {};
        data.fabric_type = fabricType;
        data.style_name = styleName
        data.hat_color = hatColor;
        data.image_url = imageUrl;
        data.location = location;

        const hatUrl = 'http://localhost:8090/api/hats/';
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(hatUrl, fetchConfig);
        if (response.ok) {
            const newHat = await response.json();
            setFabricType('');
            setStyleName('');
            setHatColor('');
            setImageUrl('');
            setLocation('');
        }
    }

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/locations/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations)
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new hat</h1>
                    <form onSubmit={handleSubmit} id="create-location-form">
                    <div className="form-floating mb-3">
                        <input onChange={handleStyleNameChange} placeholder="Name" required type="text" name="style_name" id="style_name" value={styleName} className="form-control"/>
                        <label htmlFor="style_name">Hat Style</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleHatColorChange} placeholder="Color" required type="text" name="hat_color" id="hat_color" value={hatColor} className="form-control"/>
                        <label htmlFor="hat_color">Hat Color</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleFabricTypeChange} placeholder="Fabric" required type="text" name="fabric_type" id="fabric_type" value={fabricType} className="form-control"/>
                        <label htmlFor="fabric_type">Material</label>
                    </div>
                    <div className="form-floating mb-3">
                        <input onChange={handleImageUrlChange} placeholder="Image" required type="text" name="image_url" id="image_url" value={imageUrl} className="form-control"/>
                        <label htmlFor="image_url">Image Url</label>
                    </div>
                    <div className="mb-3">
                        <select onChange={handleLocationChange} required name="location" id="location" className="form-select">
                            <option value="">Choose a Closet</option>
                            {locations.map(location => {
                                return (
                                <option key={location.href} value={location.href}>
                                    {location.closet_name}
                                </option>
                                );
                            })}
                        </select>
                    </div>
                    <button className="btn btn-info">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );

}
export default AddHat
