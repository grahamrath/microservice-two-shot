import { BrowserRouter, Routes, Route } from 'react-router-dom';
import { useState, useEffect } from "react";
import MainPage from './MainPage';
import Nav from './Nav';
import ListHats from './ListHats';
import AddHat from './AddHat';
import ShoesForm from './ShoesForm';
import ShoesList from './ShoesList';

function App(props) {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="hats">
            <Route path="list" element={<ListHats />} />
            <Route path="new" element={<AddHat />} />
          </Route>
          <Route path="shoes/new" element={<ShoesForm />} />
          <Route path="shoes/" element={<ShoesList shoes={props.shoes} />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
