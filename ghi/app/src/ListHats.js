import {useEffect, useState} from "react";

function ListHats() {
    const [hats, setHats] = useState([]);

    const deleteHat = async(id) => {
        const fetchConfig = {
            method: "DELETE",
            'Content-Type': 'application/json'
        }
        const response = await fetch (`http://localhost:8090/api/hats/${id}/`, fetchConfig)
        if (response.ok) {
            getData()
        }
    }

    const getData = async () => {
        const url = 'http://localhost:8090/api/hats/';

            const response = await fetch(url);
            if (response.ok) {
                const data = await response.json();
                setHats(data.hats);
            }
        };

    useEffect(() =>{
        getData()
    }, []);


    return (
        <div className="row m-5">
            {hats.map(hat => {
                return (
                    <div key={hat.id} className="card w-25">
                        <img src={hat.image_url} className="card-img-top p-2" alt="..."/>
                        <div className="card-body">
                        <h5 className="card-title">{hat.style_name}</h5>
                        </div>
                        <ul className="list-group list-group-flush">
                            <li className="list-group-item"> Color - {hat.hat_color}</li>
                            <li className="list-group-item"> Material - {hat.fabric_type}</li>
                            <li className="list-group-item"> Closet - {hat.closet_name}</li>
                            <li type="button" id={hat.id} onClick={() => deleteHat(hat.id)} className="btn btn-danger m-2">Delete</li>
                        </ul>
                    </div>
                );
            })}
        </div>
    );
}

export default ListHats
