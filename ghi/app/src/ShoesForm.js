import React, { useEffect, useState } from 'react';

function ShoesForm({ refreshShoes }) {
  const [manufacturer, setManufacturer] = useState('');
  const [model, setModel] = useState('');
  const [color, setColor] = useState('');
  const [picture, setPicture] = useState('');
  const [bin, setBin] = useState('');
  const [bins, setBins] = useState([]);

  const handleManufacturerChange = (event) => {
    const value = event.target.value;
    setManufacturer(value);
  }

  const handleModelChange = (event) => {
    const value = event.target.value;
    setModel(value);
  }

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  }

  const handlePictureChange = (event) => {
    const value = event.target.value;
    setPicture(value);
  }

  const handleBinChange = (event) => {
    const value = event.target.value;
    setBin(value);
  }

  const handleSubmit = async (event) => {
    event.preventDefault();

    const data = {};
    data.manufacturer = manufacturer;
    data.model = model;
    data.color = color;
    data.picture = picture;
    data.bin = bin;

    const shoesUrl = 'http://localhost:8080/api/shoes/';
    const fetchConfig = {
      method: "post",
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(shoesUrl, fetchConfig);
    if (response.ok) {
      const newShoes = await response.json();
      setManufacturer('');
      setModel('');
      setColor('');
      setPicture('');
      setBin('');
    }
  }

  const fetchData = async () => {
    const url = 'http://localhost:8080/api/bins';

    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setBins(data.bin_vos);
    }
  }

  useEffect(() => {
    fetchData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add New Shoes</h1>
          <form onSubmit={handleSubmit} id="create-shoes-form">
            <div className="form-floating mb-3">
              <input onChange={handleManufacturerChange} placeholder="Manufacturer" required type="text" id="manufacturer" className="form-control" value={manufacturer} />
              <label htmlFor="name">Manufacturer</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleModelChange} placeholder="Model" required type="text" id="model" className="form-control" value={model} />
              <label htmlFor="room_count">Model</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handleColorChange} placeholder="Color" required type="text" id="color" className="form-control" value={color} />
              <label htmlFor="city">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input onChange={handlePictureChange} placeholder="Picture" required type="text" id="picture" className="form-control" value={picture} />
              <label htmlFor="picture">Picture</label>
            </div>
            <div className="mb-3">
              <select onChange={handleBinChange} required name="bin" id="bin" className="form-select" value={bin}>
                <option value="">Choose a Bin</option>
                {bins.map(bin => {
                  return (
                    <option key={bin.import_href} value={bin.import_href}>
                      {`Bin Number ${bin.id}`}
                    </option>
                  );
                })}
              </select>
            </div>
            <div>
              <button className="btn btn-primary">Create</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
}

export default ShoesForm;
