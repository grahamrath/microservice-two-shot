import { useEffect, useState } from 'react';

function ShoesList(props) {
  const [shoes, setShoes] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/shoes/');

    if (response.ok) {
      const data = await response.json();
      setShoes(data.shoes)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  const deleteShoes = async(id) => {
    const fetchConfig = {
      method: "DELETE",
      "Content-Type" : "application/json",
    }
    const response = await fetch(`http://localhost:8080/api/shoes/${id}/`, fetchConfig)
    if (response.ok) {
      getData()
    }
  }

  return (
    <div className="container">
      <table className="table align-middle table-striped table-hover table-borderless">
        <thead>
          <tr>
            <th scope="col">Manufacturer</th>
            <th scope="col">Model</th>
            <th scope="col">Color</th>
            <th scope="col">Picture</th>
            <th scope="col">Bin</th>
            <th scope="col">Closet</th>
          </tr>
        </thead>
        <tbody>
          {shoes.map (shoe => {
            return (
              <tr key={ shoe.import_href }>
                <td>{ shoe.manufacturer }</td>
                <td>{ shoe.model }</td>
                <td>{ shoe.color }</td>
                <td><img src={ shoe.picture } className="rounded img w-25" /></td>
                <td>{ shoe.bin }</td>
                <td>{ shoe.closet }</td>
                <td>
                  <button type="button" id={shoe.id} onClick={() => deleteShoes(shoe.id)} className="btn btn-danger">
                    Delete
                  </button>
                </td>
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  )
}

export default ShoesList;
