from django.db import models


class LocationVO(models.Model):
    import_href = models.CharField(max_length=100)
    closet_name = models.CharField(max_length=100)


class Hat(models.Model):
    """
    represents a hat
    """

    fabric_type = models.CharField(max_length=100)
    style_name = models.CharField(max_length=100)
    hat_color = models.CharField(max_length=100)
    image_url = models.URLField(null=True, max_length=200)

    location = models.ForeignKey(
        LocationVO,
        related_name="hat",
        on_delete=models.CASCADE
    )
