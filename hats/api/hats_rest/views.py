from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import Hat, LocationVO


class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = [
        "import_href",
        "closet_name",
    ]


class HatEncoder(ModelEncoder):
    model = Hat
    properties = [
        "style_name",
        "id",
        "image_url",
        "style_name",
        "hat_color",
        "fabric_type",
    ]

    encoder = {
        "location": LocationVODetailEncoder
    }

    def get_extra_data(self, o):
        return {
            "import_href": o.location.import_href,
            "closet_name": o.location.closet_name
        }

@require_http_methods(["GET", "POST"])
def api_list_hats(request):
    """
    Allows you to list/create the hats
    """
    if request.method == "GET":
        hats = Hat.objects.all()
        return JsonResponse(
            {"hats": hats},
            encoder=HatEncoder,
        )
    else:  # POST
        try:
            content = json.loads(request.body)
            location = LocationVO.objects.get(import_href=content["location"])
            content["location"] = location
        except LocationVO.DoesNotExist:
            return JsonResponse(
                {"message": "Invalid ID"},
                status=400
            )
        hat = Hat.objects.create(**content)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )


@require_http_methods(["GET", "DELETE", "PUT"])
def api_detail_hats(request, pk):

    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(
            hat,
            encoder=HatEncoder,
            safe=False,
        )
    elif request.method == "DELETE":
        try:
            hat = Hat.objects.get(id=pk)
            hat.delete()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            return JsonResponse({"message": "Does not exist"})
    else:  # PUT
        try:
            content = json.loads(request.body)
            hat = Hat.objects.get(id=pk)
            location_vo = LocationVO.objects.get(import_href=content["location"])
            props = [
                "fabric_type",
                "style_name",
                "hat_color",
                "image_url",
                "location",
                ]
            for prop in props:
                if prop in content:
                    if prop == "location":
                        setattr(hat, "location", location_vo)
                        continue
                    setattr(hat, prop, content[prop])
            hat.save()
            return JsonResponse(
                hat,
                encoder=HatEncoder,
                safe=False,
            )
        except Hat.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
