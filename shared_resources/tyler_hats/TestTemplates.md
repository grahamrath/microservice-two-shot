# Test Formatting:

# HATS:

{
    "style_name": "Rabadon's Deathcap",
    "image_url": "https://lolfanatics.com/wp-content/uploads/2022/02/Rabadon.png",
    "hat_color": "Purple",
    "fabric_type": "Cloth",
    "location": "/api/locations/2/",
}

# LOCATION:

{
    "closet_name": "Viegar's Closet",
    "section_number": 3,
    "shelf_number": 3
}
