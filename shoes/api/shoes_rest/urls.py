from django.urls import path
from .views import api_list_shoes, api_shoes_details, list_bin_vos


urlpatterns = [
    path("shoes/", api_list_shoes, name="api_list_shoes"),
    path("shoes/<int:pk>/", api_shoes_details, name="api_shoes_details"),
    path("bins/", list_bin_vos, name="list_bin_vos"),
]
