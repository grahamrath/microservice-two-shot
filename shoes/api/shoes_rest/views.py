from django.shortcuts import render
from .models import BinVO, Shoes
from common.json import ModelEncoder
from django.views.decorators.http import require_http_methods
from django.http import JsonResponse
import json


class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["closet_name", "import_href", "id"]


class ShoesListEncoder(ModelEncoder):
    model = Shoes
    properties = ["manufacturer", "model", "color", "id", "picture"]

    def get_extra_data(self, o):
        return {
            "bin": o.bin.id,
            "closet": o.bin.closet_name,
            }


class ShoesDetailEncoder(ModelEncoder):
    model = Shoes
    properties = [
        "manufacturer",
        "model",
        "color",
        "picture",
        "bin",
    ]
    encoders = {
        "bin": BinVODetailEncoder(),
    }


@require_http_methods(["GET", "POST"])
def api_list_shoes(request):
    if request.method == "GET":
        shoes = Shoes.objects.all()
        return JsonResponse(
            {"shoes": shoes},
            encoder=ShoesListEncoder,
        )
    else:
        content = json.loads(request.body)
        bin = BinVO.objects.get(import_href=content["bin"])
        content["bin"] = bin
        shoes = Shoes.objects.create(**content)
        return JsonResponse(
            shoes,
            encoder=ShoesListEncoder,
            safe=False,
        )


@require_http_methods(["DELETE", "GET", "PUT"])
def api_shoes_details(request, pk):
    if request.method == "GET":
        try:
            shoes = Shoes.objects.get(id=pk)
            return JsonResponse(
                shoes,
                encoder=ShoesDetailEncoder,
                safe=False
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        count, _ = Shoes.objects.filter(id=pk).delete()
        return JsonResponse(
            {"deleted": count > 0}  # is what tells you deleted = true
            )                       # when you delete something in insomnia
    else:
        try:
            content = json.loads(request.body)
            shoes = Shoes.objects.get(id=pk)
            bin_vo = BinVO.objects.get(import_href=content["bin"])
            props = [
                "manufacturer",
                "model",
                "color",
                "picture",
                "bin"]
            for prop in props:
                if prop in content:
                    if prop == "bin":
                        setattr(shoes, "bin", bin_vo)
                        continue
                    setattr(shoes, prop, content[prop])
            shoes.save()
            return JsonResponse(
                shoes,
                encoder=ShoesDetailEncoder,
                safe=False,
            )
        except Shoes.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


@require_http_methods("GET")       # this function allows the bins dropdown
def list_bin_vos(request):         # list to get and display the Bins you can
    bin_vos = BinVO.objects.all()  # add shoes to
    return JsonResponse(
        {"bin_vos": bin_vos},
        encoder=BinVODetailEncoder,
    )
